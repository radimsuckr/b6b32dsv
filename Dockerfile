FROM rust:1.57 AS builder

WORKDIR /build

RUN rustup component add rustfmt

COPY . .

RUN cargo build --release


FROM debian:bookworm-slim

WORKDIR /app

RUN addgroup --gid 1000 dsv && adduser --uid 1000 --gid 1000 dsv

COPY --from=builder /build/target/release/semestralka-rust /app/dsv

RUN chown -R dsv:dsv /app

USER dsv:dsv

ENTRYPOINT ["/app/dsv"]
