pub mod communication {
    tonic::include_proto!("communication");
}

use crate::{client::Client, node::NetworkMapAbilities, state::State};
use communication::{network_server::Network, IpAddress, IpAddresses, Timestamp, TimestampedIpAddress, Variable};
use log::{error, info};
use std::{net::SocketAddr, str::FromStr, sync::Arc};
use tokio::sync::RwLock;
use tonic::{Request, Response, Status};

#[derive(Debug)]
pub struct AppServer {
    state: Arc<RwLock<State>>,
}

impl AppServer {
    pub fn new(state: Arc<RwLock<State>>) -> Self {
        Self { state }
    }
}

#[tonic::async_trait]
impl Network for AppServer {
    async fn announce(&self, request: Request<IpAddress>) -> Result<Response<()>, Status> {
        let message = request.get_ref();
        let origin_address = SocketAddr::from_str(&message.ip_address).unwrap();
        info!("Received announce message from: {}", origin_address);

        self.state.write().await.network_map.insert_address(&origin_address);

        let response = Response::new(());

        Ok(response)
    }

    async fn healthcheck(&self, _: Request<()>) -> Result<Response<()>, Status> {
        Ok(Response::new(()))
    }

    async fn join(&self, request: Request<IpAddress>) -> Result<Response<IpAddresses>, Status> {
        let message = request.get_ref();
        let origin_address = SocketAddr::from_str(&message.ip_address).unwrap();
        info!("Received join request from: {}", origin_address);

        let mut lock = self.state.write().await;
        lock.network_map.insert_address(&origin_address);
        let addresses = lock
            .network_map
            .iter()
            .map(|(addr, _)| addr.to_string())
            .collect::<Vec<String>>();
        let timestamp = lock.clock.current();
        drop(lock);

        let response = communication::IpAddresses {
            peer_ip_addresses: addresses.clone(),
            timestamp,
        };

        match Client::announce(&origin_address, &addresses).await {
            Ok(_) => Ok(Response::new(response)),
            Err(_) => Err(Status::unavailable(String::from("You shall not join"))),
        }
    }

    async fn leave(&self, request: Request<IpAddress>) -> Result<Response<()>, Status> {
        let message = request.get_ref();
        let origin_address = SocketAddr::from_str(&message.ip_address).unwrap();
        info!("Received leave message from: {}", origin_address);

        let mut lock = self.state.write().await;
        lock.network_map.remove(&origin_address);
        drop(lock);

        Ok(Response::new(()))
    }

    async fn reply(&self, request: Request<IpAddress>) -> Result<Response<()>, Status> {
        let message = request.get_ref();
        let origin_address = SocketAddr::from_str(&message.ip_address).unwrap();
        info!("Received reply request from: {}", origin_address);

        let mut lock = self.state.write().await;
        if let Err(error) = lock.network_map.mark_replied(&origin_address) {
            error!("Failed to mark \"{}\" as replied = {:?}", origin_address, error);
            return Err(Status::internal("Internal error. This address is not in map."));
        }
        drop(lock);

        let response = Response::new(());

        Ok(response)
    }

    async fn request(&self, request: Request<TimestampedIpAddress>) -> Result<Response<Timestamp>, Status> {
        let message = request.get_ref();
        let origin_address = SocketAddr::from_str(&message.ip_address).unwrap();
        info!("Received access request from: {}", origin_address);

        let mut lock = self.state.write().await;
        if let Err(error) = lock.network_map.defer(&origin_address, message.timestamp) {
            error!(
                "Failed to defer \"{}\" with timestamp {} = {:?}",
                origin_address, message.timestamp, error
            );
            return Err(Status::internal("Internal error. This address is not in map."));
        }
        let current_clock = lock.clock.current();
        drop(lock);

        let response = communication::Timestamp {
            timestamp: current_clock,
        };

        info!("Responding to access request from: {}", origin_address);
        Ok(Response::new(response))
    }

    async fn write(&self, request: Request<Variable>) -> Result<Response<()>, Status> {
        let message = request.get_ref();
        info!("Received write request for value: {}", message.value);

        let mut lock = self.state.write().await;
        lock.variable = message.value.clone();
        drop(lock);

        let response = Response::new(());

        info!("Responding to write request for value: {}", message.value);

        Ok(response)
    }
}
