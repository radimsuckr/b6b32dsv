use crate::{clock::LamportClock, node::NetworkMap};
use std::net::SocketAddr;

#[derive(Debug)]
pub struct State {
    pub address: SocketAddr,
    pub clock: LamportClock,
    pub is_in_critical_section: bool,
    pub is_requesting: bool,
    pub network_map: NetworkMap,
    pub variable_buffer: String,
    pub variable: String,
}

impl State {
    pub fn new(address: SocketAddr) -> Self {
        Self {
            address,
            clock: LamportClock::new(),
            is_in_critical_section: false,
            is_requesting: false,
            network_map: NetworkMap::new(),
            variable_buffer: String::new(),
            variable: String::new(),
        }
    }
}
