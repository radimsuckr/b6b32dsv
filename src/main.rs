mod client;
mod clock;
mod node;
mod server;
mod state;

use crate::{
    client::Client,
    clock::LamportClock,
    node::{NetworkMapAbilities, NodeState},
    server::{communication::network_server::NetworkServer, AppServer},
    state::State,
};
use fern::colors::{Color, ColoredLevelConfig};
use log::{error, info, warn};
use node::Node;
use std::{
    env,
    io::{self, Write},
    net::{IpAddr, SocketAddr},
    str::FromStr,
    sync::Arc,
};
use tokio::{sync::RwLock, time};
use tonic::transport::Server;

const HELP_MESSAGE: &str = r"cr | clock_reset - reset clock timestamp to 1
h  | help - this help message
i  | increment - increment 1 to current timestamp
j  | join <ip>:<port> - join the network via this node
l  | leave - leave the network and notify all neighbors
ps | print_state - print current node state
q  | quit - quit the application
r  | read - read the shared variable
tc | toggle_critical_section - toggle is_in_critical_section state
tr | toggle_requesting - toggle is_requesting state
w  | write <value> - write some value into the shared variable";

async fn background_worker(state: Arc<RwLock<State>>) {
    loop {
        let lock = state.read().await;
        let is_in_critical_section = lock.is_in_critical_section;
        let is_requesting = lock.is_requesting;
        let min_deferred = lock
            .network_map
            .clone()
            .into_iter()
            .filter(|(_, node)| node.state == NodeState::DeferredNotReplied && node.deferred_timestamp > 0)
            .min_by_key(|(_, node)| node.deferred_timestamp);
        let address = lock.address;
        let timestamp = lock.clock.current();
        drop(lock);

        if is_requesting && min_deferred != None {
            let (addr, node) = min_deferred.unwrap();
            if node.deferred_timestamp < timestamp {
                if let Err(error) = Client::reply(
                    &address,
                    &addr,
                    node.deferred_timestamp,
                    is_in_critical_section,
                    is_requesting,
                    timestamp,
                )
                .await
                {
                    error!("Error replying to \"{}\" = {:?}", addr, error);
                } else if let Err(error) = state.write().await.network_map.undefer(&addr) {
                    error!("Failed to undefer \"{}\" = {:?}", addr, error);
                    continue;
                }

                state.write().await.variable_buffer = String::new();
                continue;
            }
        }

        if is_requesting {
            // enter critical section
            {
                let lock = state.read().await;
                let all_replied = lock
                    .network_map
                    .iter()
                    .all(|elem| matches!(elem.1.state, NodeState::DeferredReplied | NodeState::NotDeferredReplied));
                drop(lock);
                if !all_replied {
                    warn!("Not all nodes replied");
                    time::sleep(time::Duration::from_millis(500)).await;
                    continue;
                }
            }

            {
                let mut lock = state.write().await;
                info!("Entering critical section");
                lock.is_in_critical_section = true;
                lock.variable = lock.variable_buffer.clone();
            }

            {
                let mut lock = state.write().await;
                let addresses = lock
                    .network_map
                    .iter()
                    .map(|(addr, _)| addr.to_string())
                    .collect::<Vec<String>>();
                let variable_buffer = lock.variable_buffer.clone();
                let current_clock = lock.clock.current();
                lock.clock.increment(current_clock).unwrap();
                drop(lock);
                info!("Broadcasting new value...");
                if let Err(error) = Client::broadcast(&addresses, &variable_buffer).await {
                    error!("Error broadcasting = {:?}", error);
                    time::sleep(time::Duration::from_millis(500)).await;
                    continue;
                }
            }
            info!("Done broadcasting");

            {
                let mut lock = state.write().await;
                lock.is_in_critical_section = false;
                lock.is_requesting = false;
                let addresses = lock
                    .network_map
                    .iter_mut()
                    .map(|(addr, _)| *addr)
                    .collect::<Vec<SocketAddr>>();
                for addr in addresses {
                    if let Err(error) = lock.network_map.mark_not_replied(&addr) {
                        error!("Failed to mark \"{}\" as not replied = {:?}", addr, error);
                    }
                }
                lock.variable_buffer = String::new();
            }
            info!("Left critical section");
            // leave critical section
        } else {
            let lock = state.read().await;
            let addresses = lock
                .network_map
                .iter()
                .filter(|(_, node)| node.state == NodeState::DeferredNotReplied)
                .map(|(addr, node)| (*addr, *node))
                .collect::<Vec<(SocketAddr, Node)>>();
            let origin_address = lock.address;
            let is_in_critical_section = lock.is_in_critical_section;
            let is_requesting = lock.is_requesting;
            let timestamp = lock.clock.current();
            drop(lock);

            for (addr, node) in addresses {
                if let Err(error) = Client::reply(
                    &origin_address,
                    &addr,
                    node.deferred_timestamp,
                    is_in_critical_section,
                    is_requesting,
                    timestamp,
                )
                .await
                {
                    error!("Error replying to \"{}\" = {:?}", addr, error);
                } else if let Err(error) = state.write().await.network_map.undefer(&addr) {
                    error!("Failed to undefer \"{}\" = {:?}", addr, error);
                    continue;
                }
            }
        }

        time::sleep(time::Duration::from_millis(500)).await;
    }
}

async fn healthchecker(state: Arc<RwLock<State>>) {
    loop {
        let addresses = &state
            .read()
            .await
            .network_map
            .iter()
            .map(|(addr, _)| *addr)
            .collect::<Vec<SocketAddr>>();
        if let Some(failed_addresses) = Client::healthcheck(addresses).await {
            let mut lock = state.write().await;
            for failed_addr in failed_addresses {
                if let Some(node) = lock.network_map.get_mut(failed_addr) {
                    node.failed_healthchecks += 1;
                    warn!(
                        "Failed healthcheck of \"{}\" = {}",
                        failed_addr, node.failed_healthchecks
                    );
                    if node.failed_healthchecks >= 3 {
                        lock.network_map.remove(failed_addr);
                        info!("Removed \"{}\" from network map", failed_addr);
                    }
                }
            }
        };

        time::sleep(time::Duration::from_secs(2)).await;
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let fern_colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .info(Color::Cyan)
        .warn(Color::Yellow);
    fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{} {}: {}",
                chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                fern_colors.color(record.level()),
                message,
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(std::io::stdout())
        .chain(fern::log_file("output.log")?)
        .apply()?;

    let args: Vec<String> = env::args().collect();
    let address = if let 3 = args.len() {
        let ip_addr = if let Ok(ip) = IpAddr::from_str(args.get(1).unwrap()) {
            ip
        } else {
            panic!("Invalid IP address \"{}\"", args.get(1).unwrap());
        };
        let port = if let Ok(port) = args.get(2).unwrap().parse::<u16>() {
            port
        } else {
            panic!("Port must be in range 0-65535");
        };

        SocketAddr::new(ip_addr, port)
    } else {
        panic!("Provide <ip> <port>")
    };

    info!("Initializing...");
    let state = Arc::new(RwLock::new(State::new(address)));
    let app_server = AppServer::new(state.clone());

    tokio::spawn(
        Server::builder()
            .add_service(NetworkServer::new(app_server))
            .serve(SocketAddr::from_str(format!("0.0.0.0:{}", address.port()).as_str()).unwrap()),
    );

    tokio::spawn(background_worker(state.clone()));

    tokio::spawn(healthchecker(state.clone()));

    loop {
        print!(">>> ");
        io::stdout().flush().expect("Failed to flush stdout");
        let mut buffer = String::new();
        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read a line from console");
        buffer = buffer.trim().to_string();
        let split_buffer = buffer.split(' ').collect::<Vec<&str>>();

        if buffer == "q" || buffer == "quit" {
            break;
        } else if buffer == "h" || buffer == "help" {
            info!("{}", HELP_MESSAGE);
        } else if buffer == "ps" || buffer == "print_state" {
            info!("{:#?}", state.read().await);
        } else if buffer == "l" || buffer == "leave" {
            let lock = state.read().await;
            if lock.network_map.is_empty() {
                error!("Not connected to any network.");
                continue;
            }
            let addresses = lock
                .network_map
                .iter()
                .map(|(addr, _)| addr.to_string())
                .collect::<Vec<String>>();
            drop(lock);

            info!("Leaving the network...");
            if let Err(error) = Client::leave(&address, &addresses).await {
                error!("{:#?}", error);
            }
            state.write().await.network_map.clear();
            info!("Left the network");
        } else if buffer == "tc" || buffer == "toggle_critical_section" {
            let current = state.read().await.is_in_critical_section;
            state.write().await.is_in_critical_section = !current;
            info!("is_in_critical_section = {}", !current);
        } else if buffer == "tr" || buffer == "toggle_requesting" {
            let current = state.read().await.is_requesting;
            state.write().await.is_requesting = !current;
            info!("is_requesting = {}", !current);
        } else if buffer == "r" || buffer == "read" {
            let current = &state.read().await.variable;
            info!("Current value is: \"{}\"", current);
        } else if buffer == "i" || buffer == "increment" {
            let mut lock = state.write().await;
            let current_clock = lock.clock.current();
            let new_clock = lock.clock.increment(current_clock).unwrap();
            drop(lock);
            info!("Timestamp changed from {} to {}", current_clock, new_clock);
        } else if buffer == "cr" || buffer == "clock_reset" {
            state.write().await.clock = LamportClock::new();
            info!("Clock reset");
        } else if split_buffer.len() == 2 {
            if split_buffer.get(0).unwrap() == &"j" || split_buffer.get(0).unwrap() == &"join" {
                let target_address = if let Ok(ip) = SocketAddr::from_str(split_buffer.get(1).unwrap()) {
                    ip
                } else {
                    error!("Invalid IP address and port pair \"{}\"", split_buffer.get(1).unwrap());
                    continue;
                };

                if target_address == address {
                    error!("Only a mad man would try to connect to themselves...");
                    continue;
                }

                match Client::join(&address, &target_address).await {
                    Ok(response) => {
                        let message = response.get_ref();
                        info!("CLIENT RESPONSE = {:#?}", message);

                        let mut lock = state.write().await;
                        if let Err(error) = lock.clock.set(message.timestamp) {
                            error!("{:?}", error);
                            continue;
                        }
                        lock.network_map.insert_address(&target_address);
                        for ip_address in message.peer_ip_addresses.iter() {
                            let addr = SocketAddr::from_str(ip_address).unwrap();
                            if addr == address {
                                continue;
                            }

                            lock.network_map.insert_address(&addr);
                        }
                    }
                    Err(error) => {
                        error!("CLIENT ERROR = {:#?}", error);
                    }
                }
            }

            if split_buffer.get(0).unwrap() == &"w" || split_buffer.get(0).unwrap() == &"write" {
                let value = split_buffer.get(1).unwrap();

                let mut lock = state.write().await;
                lock.variable_buffer = value.to_string();
                lock.is_requesting = true;

                info!("Asking for access permissions");
                let timestamp = lock.clock.current();
                let addresses = lock
                    .network_map
                    .iter()
                    .map(|(addr, _)| addr.to_string())
                    .collect::<Vec<String>>();
                drop(lock);

                match Client::request(&address, &addresses, timestamp).await {
                    Ok(_) => info!("Write succeeded"),
                    Err(error) => error!("Write failed with error = {:#?}", error),
                }
            }
        } else {
            error!("Unknown command \"{}\". Try \"help\".", buffer);
        }
    }

    Ok(())
}
