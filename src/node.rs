use std::{collections::HashMap, net::SocketAddr};

#[allow(clippy::enum_variant_names)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum NodeState {
    DeferredNotReplied,
    DeferredReplied,
    NotDeferredNotReplied,
    NotDeferredReplied,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Node {
    pub deferred_timestamp: u64,
    pub failed_healthchecks: u8,
    pub state: NodeState,
}

impl Node {
    pub fn new() -> Self {
        Self {
            deferred_timestamp: 0,
            failed_healthchecks: 0,
            state: NodeState::NotDeferredNotReplied,
        }
    }
}

pub type NetworkMap = HashMap<SocketAddr, Node>;

#[derive(Debug)]
pub enum NetworkMapError {
    AddressNotInMap,
}

pub trait NetworkMapAbilities {
    fn defer(&mut self, address: &SocketAddr, timestamp: u64) -> Result<(), NetworkMapError>;
    fn insert_address(&mut self, address: &SocketAddr) -> Option<Node>;
    fn mark_not_replied(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError>;
    fn mark_replied(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError>;
    fn undefer(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError>;
}

impl NetworkMapAbilities for NetworkMap {
    fn defer(&mut self, address: &SocketAddr, timestamp: u64) -> Result<(), NetworkMapError> {
        let node = if let Some(n) = self.get_mut(address) {
            n
        } else {
            return Err(NetworkMapError::AddressNotInMap);
        };
        node.deferred_timestamp = timestamp;
        node.state = match node.state {
            NodeState::DeferredNotReplied | NodeState::NotDeferredNotReplied => NodeState::DeferredNotReplied,
            NodeState::DeferredReplied | NodeState::NotDeferredReplied => NodeState::DeferredReplied,
        };
        Ok(())
    }

    fn insert_address(&mut self, address: &SocketAddr) -> Option<Node> {
        self.insert(*address, Node::new())
    }

    fn mark_not_replied(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError> {
        let node = if let Some(n) = self.get_mut(address) {
            n
        } else {
            return Err(NetworkMapError::AddressNotInMap);
        };
        node.state = match node.state {
            NodeState::DeferredNotReplied | NodeState::DeferredReplied => NodeState::DeferredNotReplied,
            NodeState::NotDeferredNotReplied | NodeState::NotDeferredReplied => NodeState::NotDeferredNotReplied,
        };
        Ok(())
    }

    fn mark_replied(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError> {
        let node = if let Some(n) = self.get_mut(address) {
            n
        } else {
            return Err(NetworkMapError::AddressNotInMap);
        };
        node.state = match node.state {
            NodeState::DeferredNotReplied | NodeState::DeferredReplied => NodeState::DeferredReplied,
            NodeState::NotDeferredNotReplied | NodeState::NotDeferredReplied => NodeState::NotDeferredReplied,
        };
        Ok(())
    }

    fn undefer(&mut self, address: &SocketAddr) -> Result<(), NetworkMapError> {
        let node = if let Some(n) = self.get_mut(address) {
            n
        } else {
            return Err(NetworkMapError::AddressNotInMap);
        };
        node.state = match node.state {
            NodeState::DeferredNotReplied | NodeState::NotDeferredNotReplied => NodeState::NotDeferredNotReplied,
            NodeState::DeferredReplied | NodeState::NotDeferredReplied => NodeState::NotDeferredReplied,
        };
        Ok(())
    }
}
