use tonic::Status;

#[derive(Debug)]
pub enum ClockError {
    NewValueLowerThanCurrent,
}

impl From<ClockError> for tonic::Status {
    fn from(_: ClockError) -> Self {
        Status::failed_precondition("New clock timestamp is lower than current value")
    }
}

#[derive(Debug)]
pub struct LamportClock {
    counter: u64,
}

impl LamportClock {
    pub fn new() -> Self {
        Self { counter: 1 }
    }

    pub fn current(&self) -> u64 {
        self.counter
    }

    pub fn set(&mut self, new_value: u64) -> Result<u64, ClockError> {
        if self.counter > new_value {
            return Err(ClockError::NewValueLowerThanCurrent);
        } else {
            self.counter = new_value;
        }

        Ok(self.counter)
    }

    pub fn increment(&mut self, new_value: u64) -> Result<u64, ClockError> {
        self.set(new_value + 1)
    }
}
