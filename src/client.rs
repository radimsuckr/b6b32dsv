pub mod communication {
    tonic::include_proto!("communication");
}

use communication::{network_client::NetworkClient, IpAddress, IpAddresses, TimestampedIpAddress, Variable};
use log::{error, info};
use std::{collections::HashSet, net::SocketAddr};
use tonic::{Request, Response};

#[derive(Debug)]
pub enum ClientError {
    BroadcastFailed,
    FailedToAnnounce,
    FailedToConnect,
    FailedToLeave,
    JoinRequestFailed,
    NodeInCriticalSection,
    NodeInRequestingStateAndCurrentTimestampIsLower,
    ReplyNotAcknowledged,
    WriteNotAcknowledged,
}

impl From<tonic::transport::Error> for ClientError {
    fn from(_: tonic::transport::Error) -> Self {
        Self::FailedToConnect
    }
}

#[derive(Debug)]
pub struct Client;

impl Client {
    pub async fn announce(new_address: &SocketAddr, addresses: &[String]) -> Result<(), ClientError> {
        for addr in addresses {
            if addr == &new_address.to_string() {
                continue;
            }

            let mut client = NetworkClient::connect(format!("http://{}", addr)).await?;

            let request = Request::new(IpAddress {
                ip_address: new_address.to_string(),
            });

            if client.announce(request).await.is_err() {
                return Err(ClientError::FailedToAnnounce);
            }
        }

        Ok(())
    }

    pub async fn healthcheck(network_addresses: &[SocketAddr]) -> Option<HashSet<&SocketAddr>> {
        let mut failed_addresses = HashSet::new();

        for addr in network_addresses {
            let mut client = if let Ok(c) = NetworkClient::connect(format!("http://{}", addr)).await {
                c
            } else {
                failed_addresses.insert(addr);
                continue;
            };

            let request = Request::new(());

            if client.healthcheck(request).await.is_err() {
                failed_addresses.insert(addr);
            }
        }

        if failed_addresses.is_empty() {
            None
        } else {
            Some(failed_addresses)
        }
    }

    pub async fn join(
        origin_address: &SocketAddr,
        target_address: &SocketAddr,
    ) -> Result<Response<IpAddresses>, ClientError> {
        let mut client = NetworkClient::connect(format!("http://{}", target_address)).await?;

        let request = Request::new(IpAddress {
            ip_address: origin_address.to_string(),
        });

        if let Ok(response) = client.join(request).await {
            Ok(response)
        } else {
            Err(ClientError::JoinRequestFailed)
        }
    }

    pub async fn leave(address: &SocketAddr, addresses: &[String]) -> Result<(), ClientError> {
        let mut failed_to_leave_all = false;

        for addr in addresses {
            let mut client = NetworkClient::connect(format!("http://{}", addr)).await?;

            let request = Request::new(IpAddress {
                ip_address: address.to_string(),
            });

            if client.leave(request).await.is_err() {
                failed_to_leave_all = true;
                error!("Failed to notify \"{}\" about leaving", addr);
                continue;
            }

            info!("Notified \"{}\" about leaving the network", addr);
        }

        if failed_to_leave_all {
            Err(ClientError::FailedToLeave)
        } else {
            Ok(())
        }
    }

    pub async fn reply(
        origin_address: &SocketAddr,
        target_address: &SocketAddr,
        origin_timestamp: u64,
        is_in_critical_section: bool,
        is_requesting: bool,
        timestamp: u64,
    ) -> Result<(), ClientError> {
        let mut client = NetworkClient::connect(format!("http://{}", target_address)).await?;

        if is_in_critical_section {
            return Err(ClientError::NodeInCriticalSection);
        }
        if is_requesting && origin_timestamp >= timestamp {
            return Err(ClientError::NodeInRequestingStateAndCurrentTimestampIsLower);
        }

        let request = Request::new(IpAddress {
            ip_address: origin_address.to_string(),
        });
        if client.reply(request).await.is_err() {
            return Err(ClientError::ReplyNotAcknowledged);
        } else {
            info!("\"{}\" acknowledged reply", target_address);
        }

        Ok(())
    }

    pub async fn broadcast(addresses: &[String], value: &str) -> Result<(), ClientError> {
        for addr in addresses.iter() {
            let mut client = NetworkClient::connect(format!("http://{}", addr)).await?;
            let request = Request::new(Variable {
                value: value.to_string(),
            });
            if client.write(request).await.is_err() {
                return Err(ClientError::BroadcastFailed);
            }
        }

        Ok(())
    }

    pub async fn request(source_address: &SocketAddr, addresses: &[String], timestamp: u64) -> Result<(), ClientError> {
        // ask for access permission
        for addr in addresses {
            info!("Access request loop to \"{}\"", addr);
            let mut client = NetworkClient::connect(format!("http://{}", addr)).await?;

            let request = Request::new(TimestampedIpAddress {
                ip_address: source_address.to_string(),
                timestamp,
            });
            info!("Request: {:?}", request);

            if client.request(request).await.is_err() {
                return Err(ClientError::WriteNotAcknowledged);
            } else {
                info!("\"{}\" acknowledged write", addr);
            }
        }
        info!("Done asking");
        // all peers acknowledged

        Ok(())
    }
}
