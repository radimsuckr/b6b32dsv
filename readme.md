# Setup

This project requires:

- Rust compiler in version compatible with 2021 edition (latest)
- Cargo (Rust package manager)

It's built mainly around Rust gRPC library tonic and directly depends on following crates:

- [chrono](https://crates.io/crates/chrono)
- [fern](https://crates.io/crates/fern)
- [log](https://crates.io/crates/log)
- [prost](https://crates.io/crates/prost)
- [tokio](https://crates.io/crates/tokio)
- [tonic](https://crates.io/crates/tonic)

## Installing Rust

The easiest ways are:

- [Rustup](https://rustup.rs/)
- Distribution package (if you're on Linux)

## Running

If you have Docker, you can download the image [`radimsuckr/dsv:latest`](https://hub.docker.com/repository/docker/radimsuckr/dsv) which contains latest version of this project. If not, you can continue below for instructions to build this project.

The app itself takes 2 arguments from CLI. First argument is IP address of the network device that should be used for communication and second argument is port which the app should listen on.

After that, the controls should be straightforward. If hesitant, use `h` or `help` commands.

### Example

`docker run --rm -it radimsuckr/dsv:latest 1.2.3.4 56789` would run the app on IP `1.2.3.4` and port `56789`. Also the `--rm` flag means that the Docker container will be removed after the app exits and `-it` means that Docker will attach terminal STDIN to Docker STDIN so it can take commands from the user input.

## Building

### Direct build

There's nothing easier than to simply run `cargo build --release` in the root of the project (if you want debug build, just omit the `--release` flag).

### Docker build

The repository also contains a `Dockerfile` prepared and used to build the Docker image on Docker Hub. To use it, run `docker build -t <image-name>:latest .`.

## Implementation details

This project implements (or at least tries to) deferring version of [Ricart-Agrawala algorithm for mutual exclusion](https://en.wikipedia.org/wiki/Ricart%E2%80%93Agrawala_algorithm). Nodes use gRPC for communication.

### Nodes

Each node remembers its own IP address and port, current [Lamport timestamp](https://en.wikipedia.org/wiki/Lamport_timestamp), whether it's currently requesting access to the shared variable or if it's in critical section, value of the shared variable and a map consisting of pairs of IP address and port and last known state of the node on this address (timestamp of deferred request for this node, count of failed healthchecks and latest state).

#### Node state

A node in the network map of the current node can be in one of the following states:

- Deferred and not replied
- Deferred and replied
- Not deferred and not replied
- Not deferred and replied

### Network topology

The chosen topology is the simplest full mesh where every node knows every other node in the network. This was chosen because of the small scale of this project.

#### Building the network

Each node can send "join" message to any other node with known address. A network is founded when the first 2 nodes connect to each other. Any subsequent joins from new nodes are just added in the current network.

When a 3rd node `C` joins the network between nodes `A` and `B` and sends the join command to `B`, `B` then broadcasts a "announce" message to whole network so that other members of the network can add `C` into their network maps.

#### Health checks

Every node sends a "health check" message to every other node every 2 seconds since its startup. If any node fails to respond to the health check, its count of failed requests is incremented by 1. If the count reaches 3, the node is marked as unreachable and removed from the network map.

#### Rebuilding the network

There is no automated way of reconnecting nodes which failed the health checks. The node must send a new "join" message.

### Ricart-Agrawala algorithm

The app does not contain any automated or periodic way of invoking the algorithm. Instead, there is a `w` or `write` command which invokes it.

When a user confirms a "write" command, the app proceeds to ask every other node on the network for its approval for the exclusive access to the shared variable by sending a "access" message. If a node wants to acknowledge the "access" request, it sends a "reply" message back to the original node. The "reply" messages are sent by the rules of the Ricart-Agrawala algorithm (simply put when the node is not requesting nor is in a critical section).

When a requesting node receives a "reply" message, it can then proceed with entering the critical section, does some computation (it just saves the variable value) and broadcasts the new value of the shared variable via a "broadcast" message to the whole network.

Below is a simplified sequence diagram of the algorithm.

![Sequence diagram of Ricart-Agrawala algorithm](./diagrams/sequence.svg)
